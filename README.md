# Sparks

Sparks is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

<<<<<<< HEAD
# returns 'local'
foobar.pluralize('local')
=======
# returns 'server'
foobar.pluralize('server')
>>>>>>> refs/remotes/origin/master

# returns 'geese'
foobar.pluralize('goose')

<<<<<<< HEAD
# returns 'local'
foobar.singularize('local')
=======
# returns 'server'
foobar.singularize('server')
>>>>>>> refs/remotes/origin/master
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.